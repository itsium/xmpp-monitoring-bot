require 'net/ping'

class HTTPPingMonitor
  def self.run(params)
    unless Net::Ping::HTTP.new(params[:domain], params[:port], params[:timeout]).ping?
      raise "#{params[:domain]} doesn't respond to HTTP requests on port #{params[:port]} after #{params[:timeout]} seconds."
    end
  end
end

