require 'net/ping'

class PingMonitor
  def self.run(params)
    unless Net::Ping::External.new(params[:domain], 0, params[:timeout]).ping?
      raise "#{params[:domain]} doesn't respond to ping after #{params[:timeout]} seconds."
    end
  end
end
