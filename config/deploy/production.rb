set :user, 'xmpp_monitoring_bot'

set :application, 'xmpp_monitoring_bot'
set :deploy_to,   '/home/xmpp_monitoring_bot'
set :env_file,    './config/environments/production.env'
set :servername,  'porygon'

server "#{user}@#{servername}", :app
