require 'capistrano/ext/multistage'
require 'capistrano/foreman'

default_run_options[:pty] = true

set :default_shell, :bash

set :application, "xmpp_monitoring_bot"
set :repository,  "git@git.itsium.cn:phorque/xmpp-monitoring-bot.git"
set :scm, :git
set :use_sudo, false
set :normalize_asset_timestamps, false

before "deploy:restart", "deploy:compile"
after  "deploy:restart", "deploy:cleanup"

set :foreman_sudo, 'sudo'
set :foreman_upstart_path, '/etc/init/'
set :foreman_options do
  {
    app: application,
    log: "#{shared_path}/log",
    user: 'xmpp_monitoring_bot'
  }
end

namespace :deploy do
  task :compile do
    run "cd #{current_path} && bundle install"
    foreman.export
  end

  task :start do
  end

  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    foreman.restart
  end
end
